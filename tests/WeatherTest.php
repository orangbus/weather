<?php
/**
 * Created by OrangBus
 * User email: orangbus40400@gmail.com
 * website: orangbus.cn
 * blog: doc.orangbus.cn
 * github: github.com/orangbus
 */

namespace Orangbus\Weather\Tests;

use GuzzleHttp\Client;
use http\Client\Response;
use Mockery\Mock;
use Orangbus\Weather\Exceptions\InvalidArgumentException;
use Orangbus\Weather\Weather;
use PHPUnit\Framework\TestCase;

class WeatherTest extends TestCase
{
    // $type 参数检查
    public function testGetWeatherWithInvalidType()
    {
        $w = new Weather("mock-key");

        $this->expectException(InvalidArgumentException::class);

        $this->expectExceptionMessage("无效的参数 type(base/all): foo");

        $w->getWeather("昆明","foo");

        $this->fail("Failed to assert getWeather throw exception with invalid argument.");
    }

    // 检查 $format 参数
    public function testGetWeatherWithInvalidFormat()
    {
        $w = new Weather("orangbus");
        $this->expectException(InvalidArgumentException::class);

        $this->expectExceptionMessage("无效的参数 format: array");

        $w->getWeather("昆明","base","array");

        $this->fail("Failed to assert getWeather throw exception with invalid argument.");
    }

    // 依赖模拟
    public function testGetWeather()
    {
        // 创建模拟接口的响应值
        $response = new Response(200,[],'{"success":"true"}');

        // 创建模拟请求
        $client = \Mockery::mock(Client::class);

        // 置顶将会产生的行为
        $client->allows()->get("https://restapi.amap.com/v3/weather/weatherInfo",[
            'query' => [
                "key" => "orangbus",
                "city" => "昆明",
                "output" => 'json',
                "extendions" => "base"
            ]
        ])->addReturn($response);
        $w = \Mockery::mock(Weather::class, ['mock-key'])->makePartial();
        $w->allows()->getHttpClient()->andReturn($client); // $client 为上面创建的模拟实例。

        // 然后调用 `getWeather` 方法，并断言返回值为模拟的返回值。
        $this->assertSame(['success' => true], $w->getWeather('深圳'));
    }


}