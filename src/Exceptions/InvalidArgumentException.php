<?php
/**
 * Created by OrangBus
 * User email: orangbus40400@gmail.com
 * website: orangbus.cn
 * blog: doc.orangbus.cn
 * github: github.com/orangbus
 */

namespace Orangbus\Weather\Exceptions;

/**
 * 无效参数异常
 * Class InvalidArgumentException
 * @package Orangbus\Weather\Exceptions
 */
class InvalidArgumentException extends Exception
{

}