<?php
/**
 * Created by OrangBus
 * User email: orangbus40400@gmail.com
 * website: orangbus.cn
 * blog: doc.orangbus.cn
 * github: github.com/orangbus
 */

namespace Orangbus\Weather\Exceptions;

/**
 * 请求异常
 * Class HttpException
 * @package Orangbus\Weather\Exceptions
 */
class HttpException extends Exception
{

}