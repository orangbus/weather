<?php
/**
 * Created by OrangBus
 * User email: orangbus40400@gmail.com
 * website: orangbus.cn
 * blog: doc.orangbus.cn
 * github: github.com/orangbus
 */

namespace Orangbus\Weather;


use GuzzleHttp\Client;
use http\Encoding\Stream;
use Orangbus\Weather\Exceptions\Exception;
use Orangbus\Weather\Exceptions\HttpException;
use Orangbus\Weather\Exceptions\InvalidArgumentException;

class Weather
{
    protected $key;
    protected $guzzleOption = [];

    public function __construct(string $key)
    {
        $this->key = $key;
    }

    public function getHttpClient()
    {
        return new Client($this->guzzleOption);
    }

    public function setGuzzleOption(array $option)
    {
        $this->guzzleOption = $option;
    }

    public function getWeather($city,$type="base",$format = 'json')
    {
        $url = 'https://restapi.amap.com/v3/weather/weatherInfo';

        if (!in_array(strtolower($format),["xml","json"])){
            throw new InvalidArgumentException("无效的参数 format: ".$format);
        }

        if (!in_array(strtolower($type),["base","all"])) {
            throw new InvalidArgumentException("无效的参数 type(base/all): ".$type);
        }

        $query = array_filter([
            "key" => $this->key,
            "city" => $city,
            "output" => $format,
            "extensions" => $type
        ]);

        try {
            $response = $this->getHttpClient()->get($url,[
                "query" => $query
            ])->getBody()->getContents();
            return 'json' == $format ? json_encode($response,true) : $response;
        }catch (Exception $e){
            throw new HttpException($e->getMessage(),$e->getCode(),$e);
        }
    }

}